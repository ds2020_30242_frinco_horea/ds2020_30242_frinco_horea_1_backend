package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.MedicationPlan;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, UUID> {
    @Query(value = "SELECT medp, med, pt " +
            "FROM Medication AS med INNER JOIN MedicationPlan AS medp ON (med.medicationId = medp.medication.medicationId) INNER JOIN Patient AS pt ON (medp.patient.patientId = pt.patientId) " +
            "WHERE pt.userr.username = :username ")
    List<MedicationPlan> getPatientMedicationPlans(@Param("username") String username);

    @Transactional
    @Modifying
    void deleteByPatientUserrUsername(String username);

    @Transactional
    @Modifying
    void deleteByMedicationMedicationId(UUID medicationId);
}
