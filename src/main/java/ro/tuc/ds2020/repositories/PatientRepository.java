package ro.tuc.ds2020.repositories;

import javassist.bytecode.ByteArray;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Patient;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface PatientRepository extends JpaRepository<Patient, UUID> {
    @Query(value = "SELECT p " +
            "FROM Patient p INNER JOIN Userr u ON (p.userr.username = u.username) " +
            "WHERE u.username = :username ")
    Patient getPatientByUsername(@Param("username") String username);

    @Query(value = "SELECT p " +
            "FROM Patient p INNER JOIN Caregiver cg ON (p.caregiver.caregiverId = cg.caregiverId) " +
            "WHERE cg.userr.username = :username ")
    List<Patient> getCaregiverPatients(@Param("username") String username);

    @Transactional
    @Modifying
    void deleteByUserrUsername(String username);
}
