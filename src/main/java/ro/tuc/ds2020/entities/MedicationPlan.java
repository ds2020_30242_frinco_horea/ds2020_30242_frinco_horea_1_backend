package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class MedicationPlan implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    @Column(name = "medication_plan_id", nullable = false)
    private UUID medicationPlanId;

    @Column(name = "interval", nullable = false)
    private String interval;

    @Column(name = "duration", nullable = false)
    private String duration;

    @ManyToOne
    @JoinColumn(name = "medicationId")
    private Medication medication;

    @ManyToOne
    @JoinColumn(name = "patientId")
    private Patient patient;

    public MedicationPlan() {
    }

    public MedicationPlan(String interval, String duration) {
        this.interval = interval;
        this.duration = duration;
    }

    public UUID getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(UUID medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
