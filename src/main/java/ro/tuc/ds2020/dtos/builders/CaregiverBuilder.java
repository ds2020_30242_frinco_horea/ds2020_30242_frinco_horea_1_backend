package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getCaregiverId(), caregiver.getName(), caregiver.getBirthDate(), caregiver.getGender(), caregiver.getAddress(), caregiver.getUserr());
    }

    public static Caregiver toEntity(CaregiverDetailsDTO caregiverDetailsDTO) {
        return new Caregiver(caregiverDetailsDTO.getName(), caregiverDetailsDTO.getBirthDate(), caregiverDetailsDTO.getGender(), caregiverDetailsDTO.getAddress(), caregiverDetailsDTO.getUsername(), caregiverDetailsDTO.getPassword(), "caregiver");
    }
}

