package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.AlertDetails;
import ro.tuc.ds2020.entities.Activity;

import java.text.*;
import java.util.Date;

public class ActivityBuilder {

    private ActivityBuilder() {
    }

    public static AlertDetails toAlertDetails(Activity activity) {
        return new AlertDetails(activity.getPatient().getPatientId(), activity.getActivityName(),  activity.getStartDate().toString(), activity.getEndDate().toString());
    }

    public static Activity toEntity(AlertDetails alertDetails) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = dateFormat.parse(alertDetails.getStartDate());
        Date endDate = dateFormat.parse((alertDetails.getEndDate()));
        return new Activity(alertDetails.getActivity(), startDate, endDate);
    }

}
