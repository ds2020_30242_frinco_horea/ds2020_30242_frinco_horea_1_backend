package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.Patient;


public class PatientBuilder {

    private PatientBuilder() {
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO) {
        return new Patient(patientDetailsDTO.getName(), patientDetailsDTO.getBirthDate(), patientDetailsDTO.getGender(), patientDetailsDTO.getAddress(), patientDetailsDTO.getMedicalRecord());
    }
}
