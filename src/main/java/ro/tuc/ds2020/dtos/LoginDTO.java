package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import java.util.Objects;
import java.util.UUID;

public class LoginDTO extends RepresentationModel<LoginDTO> {
    private String username;
    private String password;

    public LoginDTO() {
    }

    public LoginDTO(String username, String password, UUID id) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginDTO loginDTO = (LoginDTO) o;
        return Objects.equals(username, loginDTO.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }
}
