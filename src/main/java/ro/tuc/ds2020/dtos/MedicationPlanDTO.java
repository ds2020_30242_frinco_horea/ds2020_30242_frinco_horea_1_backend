package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> {

    private UUID medicationPlanId;
    @NotNull
    private UUID medicationId;
    @NotNull
    private UUID patientId;
    @NotNull
    private String interval;
    @NotNull
    private String duration;

    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(UUID medicationId, UUID patientId, String interval, String duration) {
        this.medicationId = medicationId;
        this.patientId= patientId;
        this.interval = interval;
        this.duration = duration;
    }

    public UUID getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(UUID medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public UUID getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(UUID medicationId) {
        this.medicationId = medicationId;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDTO medicationPlanDTO = (MedicationPlanDTO) o;
        return medicationPlanId == medicationPlanDTO.medicationPlanId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(interval, medicationPlanId, duration);
    }
}
