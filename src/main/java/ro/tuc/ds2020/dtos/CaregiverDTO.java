package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Userr;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class CaregiverDTO extends RepresentationModel<CaregiverDTO> {

    @NotNull
    private UUID caregiverId;
    @NotNull
    private String name;
    @NotNull
    private Date birthDate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    private Userr userr;

    public CaregiverDTO() {
    }

    public CaregiverDTO(UUID caregiverId, String name, Date birthDate, String gender, String address) {
        this.caregiverId = caregiverId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public CaregiverDTO(UUID caregiverId, String name, Date birthDate, String gender, String address, Userr userr) {
        this.caregiverId = caregiverId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.userr = userr;
    }

    public UUID getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(UUID caregiverId) {
        this.caregiverId = caregiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setAddress(String address) { this.address = address; }

    public String getAddress() {
        return address;
    }

    public Userr getUserr() {
        return userr;
    }

    public void setUserr(Userr userr) {
        this.userr = userr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return caregiverId == caregiverDTO.caregiverId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, gender, birthDate);
    }
}
