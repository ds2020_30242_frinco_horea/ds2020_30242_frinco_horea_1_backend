package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Caregiver;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class PatientDTO extends RepresentationModel<PatientDTO> {
    private UUID patientId;
    private String username;
    private String name;
    private Date birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private UUID caregiverId;

    public PatientDTO() {
    }

    public PatientDTO(String username, String name, Date birthDate, String gender, String address, String medicalRecord) {
        this.username = username;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public PatientDTO(String username, String name, Date birthDate, String gender, String address, UUID caregiverId) {
        this.username = username;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.caregiverId = caregiverId;
    }

    public PatientDTO(String username, String name, Date birthDate, String gender, String address, String medicalRecord, UUID caregiverId) {
        this.username = username;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiverId = caregiverId;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID id) {
        this.patientId = patientId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date age) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public UUID getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(UUID caregiverId) {
        this.caregiverId = caregiverId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return patientId == patientDTO.patientId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, name, gender, address, birthDate);
    }
}
