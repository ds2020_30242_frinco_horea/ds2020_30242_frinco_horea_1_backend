package ro.tuc.ds2020.dtos;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.UUID;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = AlertDetails.class)
public class AlertDetails {
    private String caregiverUsername;
    private String message;
    private UUID patientId;
    private String activity;
    private String startDate;
    private String endDate;

    public AlertDetails() {
    }

    public AlertDetails(UUID patientId, String activity, String startDate, String endDate) {
        this.patientId = patientId;
        this.activity = activity;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getCaregiverUsername() {
        return caregiverUsername;
    }

    public void setCaregiverUsername(String caregiverUsername) {
        this.caregiverUsername = caregiverUsername;
    }

    public String getMessage() {
        return message;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
