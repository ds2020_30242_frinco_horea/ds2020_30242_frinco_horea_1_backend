package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.LoginDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.Userr;
import ro.tuc.ds2020.services.UserService;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/login")
public class LoginController {

    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping()
    public ResponseEntity<UserDTO> login(@RequestBody LoginDTO loginDTO) {
        try
        {
            UserDTO userDTO = userService.findUserByUsername(loginDTO.getUsername());
            Userr u = UserBuilder.toEntity(userDTO);

            if (loginDTO.getPassword().equals((u.getPassword()))) {
                return new ResponseEntity<>(userDTO, HttpStatus.OK);
            }

            return new ResponseEntity<>(userDTO, HttpStatus.NOT_FOUND);
        }
        catch (ResourceNotFoundException ex)
        {
            return new ResponseEntity<>(new UserDTO(), HttpStatus.NOT_FOUND);
        }
    }
}
