package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Userr;
import ro.tuc.ds2020.services.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final PatientService patientService;
    private final UserService userService;
    private final MedicationPlanService medicationPlanService;
    private final CaregiverService caregiverService;
    private final MedicationService medicationService;
    private final DoctorService doctorService;

    @Autowired
    public DoctorController(PatientService patientService, UserService userService, MedicationPlanService medicationPlanService, CaregiverService caregiverService, MedicationService medicationService, DoctorService doctorService) {
        this.patientService = patientService;
        this.userService = userService;
        this.medicationPlanService = medicationPlanService;
        this.caregiverService = caregiverService;
        this.medicationService = medicationService;
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/patients")
    public ResponseEntity<List<Patient>> getAllPatients() {
        try
        {
            List<Patient> patients = patientService.getAllPatients();
            return new ResponseEntity<>(patients, HttpStatus.OK);
        }
        catch (ResourceNotFoundException ex)
        {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/patients")
    public ResponseEntity<Boolean> deletePatientByUsername(@RequestParam("name") String username) {
        try
        {
            medicationPlanService.deleteByPatientUsername(username);
            patientService.deletePatientByUsername(username);
            userService.deleteByPatientUsername(username);

            return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
        }
        catch (ResourceNotFoundException ex)
        {
            return new ResponseEntity<>(Boolean.FALSE, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/patients")
    public ResponseEntity<UUID> insertPatient(@RequestBody PatientDetailsDTO patientDetailsDTO) {
        Userr userr = userService.insertUser(patientDetailsDTO);
        UUID patientId = patientService.insertPatient(patientDetailsDTO);
        return new ResponseEntity<>(patientId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/patients")
    public ResponseEntity<UUID> updatePatient(@RequestBody PatientDetailsDTO patientDetailsDTO) {
        UUID patientId = patientService.updatePatient(patientDetailsDTO);
        return new ResponseEntity<>(patientId, HttpStatus.CREATED);
    }

    @GetMapping(value = "/caregivers")
    public ResponseEntity<List<CaregiverDTO>> getAllCaregivers() {
        List<CaregiverDTO> caregiverDTOS = caregiverService.getAllCaregivers();
        return new ResponseEntity<>(caregiverDTOS, HttpStatus.OK);
    }

    @DeleteMapping(value = "/caregivers")
    public ResponseEntity<Boolean> deleteCaregiverByUsername(@RequestParam("name") String username) {
        caregiverService.deleteCaregiverByUsername(username);
        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
    }

    @PostMapping(value = "/caregivers")
    public ResponseEntity<UUID> insertCaregiver(@RequestBody CaregiverDetailsDTO caregiverDetailsDTO) {
        Userr userr = userService.insertUser(caregiverDetailsDTO);
        UUID caregiverId = caregiverService.insert(caregiverDetailsDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/caregivers")
    public ResponseEntity<UUID> updateCaregiver(@RequestBody CaregiverDetailsDTO caregiverDetailsDTO) {
        UUID caregiverId = caregiverService.update(caregiverDetailsDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.CREATED);
    }

    @GetMapping(value = "/medication")
    public ResponseEntity<List<MedicationDTO>> getAllMedication() {
        List<MedicationDTO> medicationDTOS = medicationService.getAllMedication();
        return new ResponseEntity<>(medicationDTOS, HttpStatus.OK);
    }

    @DeleteMapping(value = "/medication")
    public ResponseEntity<Boolean> deleteMedicationByName(@RequestParam("name") String name) {
        medicationService.deleteMedicationByName(name);
        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
    }

    @PostMapping(value = "/medication")
    public ResponseEntity<UUID> insertMedication(@RequestBody MedicationDTO medicationDTO) {
        UUID medicationId = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/medication")
    public ResponseEntity<UUID> updateMedication(@RequestBody MedicationDTO medicationDTO) {
        UUID medicationId = medicationService.update(medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.CREATED);
    }

    @PostMapping(value = "/medicationPlan")
    public ResponseEntity<UUID> insertMedicationPlan(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        UUID medicationPlanId = medicationPlanService.insert(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanId, HttpStatus.CREATED);
    }

    @PostMapping(value = "")
    public ResponseEntity<UUID> insertDoctor() {
        UUID doctorId = doctorService.insert();
        return new ResponseEntity<>(doctorId, HttpStatus.CREATED);
    }
}

