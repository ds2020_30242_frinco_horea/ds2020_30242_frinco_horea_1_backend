package ro.tuc.ds2020;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.AlertDetails;
import ro.tuc.ds2020.services.MessageService;

@Component
public class QueueConsumer {
    @Autowired
    private MessageService messageService;

    @RabbitListener(queues = "${spring.rabbitmq.queue}", containerFactory = "jsaFactory")
    //@RabbitListener(queues = "Activity_Queue", containerFactory = "jsaFactory")
    public void receiveMessage(AlertDetails alertDetails) throws InterruptedException {
        System.out.println("Received " + alertDetails);
        messageService.sendMessage(alertDetails);
    }
}
