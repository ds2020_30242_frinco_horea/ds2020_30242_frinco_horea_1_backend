package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Userr;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;

    @Autowired
    public CaregiverService(PatientRepository patientRepository, CaregiverRepository caregiverRepository, UserRepository userRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    public List<Patient> getCorrespondingPatients(String username) {
        return patientRepository.getCaregiverPatients(username);
    }

    public List<CaregiverDTO> getAllCaregivers() {
        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public void deleteCaregiverByUsername(String username) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findByUserrUsername(username);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with username {} was not found in db", username);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with username: " + username);
        }
        caregiverRepository.delete(caregiverOptional.get());
        userRepository.deleteByUsername(caregiverOptional.get().getUserr().getUsername());
    }

    public UUID insert(CaregiverDetailsDTO caregiverDetailsDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDetailsDTO);
        caregiver = caregiverRepository.save(caregiver);
        return caregiver.getCaregiverId();
    }

    public UUID update(CaregiverDetailsDTO caregiverDetailsDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDetailsDTO);
        caregiver.setCaregiverId(caregiverDetailsDTO.getCaregiverId());
        caregiver = caregiverRepository.save(caregiver);
        return caregiver.getCaregiverId();
    }
}
