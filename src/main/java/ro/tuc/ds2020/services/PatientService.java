package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Userr;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public Patient getPatientByUsername(String username) {
        return patientRepository.getPatientByUsername(username);
    }

    public List<Patient> getAllPatients() { return patientRepository.findAll();
    }

    public void deletePatientByUsername(String username) {
            patientRepository.deleteByUserrUsername(username);
    }

    public UUID insertPatient(PatientDetailsDTO patientDetailsDTO) {
        UUID caregiverId = patientDetailsDTO.getCaregiverId();
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(caregiverId);

        if(!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id " + caregiverId);
        }

        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        patient.setCaregiver(caregiverOptional.get());
        patient.setUserr(new Userr(patientDetailsDTO.getUsername(), patientDetailsDTO.getPassword(), patientDetailsDTO.getRole()));
        patient = patientRepository.save(patient);
        return patient.getPatientId();
    }

    public UUID updatePatient(PatientDetailsDTO patientDetailsDTO) {
        UUID caregiverId = patientDetailsDTO.getCaregiverId();
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(caregiverId);
        if(!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id " + caregiverId);
        }
        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        patient.setPatientId(patientDetailsDTO.getPatientId());
        patient.setCaregiver(caregiverOptional.get());
        patient.setUserr(UserBuilder.toEntity(patientDetailsDTO));
        patient = patientRepository.save(patient);
        return patient.getPatientId();
    }
}
