package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Userr;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.UUID;

@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final DoctorRepository doctorRepository;
    private final UserRepository userRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository, UserRepository userRepository) {
        this.doctorRepository = doctorRepository;
        this.userRepository = userRepository;
    }

    public UUID insert() {
        Userr userr = new Userr("qwe", "qwe", "doctor");
        Doctor doctor = new Doctor();
        doctor.setUserr(userr);
        userRepository.save(userr);
        return doctorRepository.save(doctor).getDoctorId();
    }
}
