package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.AlertDetails;
import ro.tuc.ds2020.dtos.builders.ActivityBuilder;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.ActivityRepository;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.text.ParseException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class MessageService {

    @Autowired
    private SimpMessagingTemplate websocket;
    private final PatientRepository patientRepository;
    private final ActivityRepository activityRepository;

    public MessageService(PatientRepository patientRepository, CaregiverRepository caregiverRepository, ActivityRepository activityRepository) {
        this.patientRepository = patientRepository;
        this.activityRepository = activityRepository;
    }

    public void sendMessage(AlertDetails alertDetails) {
        Activity activity = insertActivity(alertDetails);

        boolean alert = false;

        if (alertDetails.getActivity().trim().equals("Sleeping"))
        {
            if (TimeUnit.HOURS.convert(Math.abs(activity.getEndDate().getTime() - activity.getStartDate().getTime()), TimeUnit.MILLISECONDS) > 7)
            {
                alert = true;
            }
        }
        else if (alertDetails.getActivity().trim().equals("Leaving"))
        {
            if (TimeUnit.HOURS.convert(Math.abs(activity.getEndDate().getTime() - activity.getStartDate().getTime()), TimeUnit.MILLISECONDS) > 5)
            {
                alert = true;
            }
        }
        else if (alertDetails.getActivity().trim().equals("Toileting"))
        {
            if (TimeUnit.MINUTES.convert(Math.abs(activity.getEndDate().getTime() - activity.getStartDate().getTime()), TimeUnit.MILLISECONDS) > 30)
            {
                alert = true;
            }
        }

        if (alert)
        {
            alertDetails.setCaregiverUsername(activity.getPatient().getCaregiver().getUserr().getUsername());
            alertDetails.setMessage(activity.getPatient().getName() + " spent " + TimeUnit.MINUTES.convert(Math.abs(activity.getEndDate().getTime() - activity.getStartDate().getTime()), TimeUnit.MILLISECONDS) + " minutes doing: " + activity.getActivityName());
            websocket.convertAndSend("/topic/alerts", alertDetails);
        }
    }

    public Activity insertActivity(AlertDetails alertDetails) {
        Activity activity = null;
        try {
            activity = ActivityBuilder.toEntity(alertDetails);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Optional<Patient> patientOptional = patientRepository.findById(alertDetails.getPatientId());

        if(!patientOptional.isPresent()) {
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id " + alertDetails.getPatientId());
        }

        Patient patient = patientOptional.get();
        activity.setPatient(patient);
        activityRepository.save(activity);
        return activity;
    }
}
