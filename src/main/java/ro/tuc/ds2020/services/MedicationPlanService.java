package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MedicationPlanService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository, PatientRepository patientRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
        this.patientRepository = patientRepository;
    }

    public UUID insert(MedicationPlanDTO medicationPlanDTO) {
        Optional<Medication> medicationOptional = medicationRepository.findById(medicationPlanDTO.getMedicationId());
        Optional<Patient> patientOptional = patientRepository.findById(medicationPlanDTO.getPatientId());

        if(!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", medicationPlanDTO.getMedicationId());
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + medicationPlanDTO.getMedicationId());
        }

        if(!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", medicationPlanDTO.getPatientId());
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + medicationPlanDTO.getPatientId());
        }

        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationPlan.setMedication(medicationOptional.get());
        medicationPlan.setPatient(patientOptional.get());
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        return medicationPlan.getMedicationPlanId();
    }

    public List<MedicationPlan> getPatientMedicationPlans(String username) {
        List<MedicationPlan> medicationPlans = medicationPlanRepository.getPatientMedicationPlans(username);
        return medicationPlans;
    }

    public void deleteByPatientUsername(String username) {
        medicationPlanRepository.deleteByPatientUserrUsername(username);
    }
}
