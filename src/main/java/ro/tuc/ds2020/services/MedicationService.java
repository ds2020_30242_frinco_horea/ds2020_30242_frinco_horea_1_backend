package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository, MedicationPlanRepository medicationPlanRepository) {
        this.medicationRepository = medicationRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }


    public List<MedicationDTO> getAllMedication() {
        List<Medication> medications = medicationRepository.findAll();
        return medications.stream()
                .map(MedicationBuilder::toMedicationDTOWithId)
                .collect(Collectors.toList());
    }

    public void deleteMedicationByName(String name) {
        Optional<Medication> medicationOptional = medicationRepository.findByName(name);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with name {} was not found in db", name);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with name: " + name);
        }
        medicationPlanRepository.deleteByMedicationMedicationId(medicationOptional.get().getMedicationId());
        medicationRepository.delete(medicationOptional.get());
    }

    public UUID insert(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        return medicationRepository.save(medication).getMedicationId();
    }

    public UUID update(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication.setMedicationId(medicationDTO.getMedicationId());
        return medicationRepository.save(medication).getMedicationId();
    }
}

