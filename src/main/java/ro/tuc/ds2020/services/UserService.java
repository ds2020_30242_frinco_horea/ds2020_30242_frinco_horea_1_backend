package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.DuplicateResourceException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.Userr;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDTO> findUsers() {
        List<Userr> userrList = userRepository.findAll();
        return userrList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public UserDTO findUserByUsername(String username) {
        Optional<Userr> userOptional = userRepository.findByUsername(username);
        if (!userOptional.isPresent()) {
            LOGGER.error("User with username {} was not found in db", username);
            throw new ResourceNotFoundException(Userr.class.getSimpleName() + " with username: " + username);
        }
        return UserBuilder.toUserDTO(userOptional.get());
    }

    public void deleteByPatientUsername(String username) {
            userRepository.deleteByUsername(username);
    }

    public Userr insertUser(PatientDetailsDTO patientDetailsDTO) {
        Userr userr = UserBuilder.toEntity(patientDetailsDTO);
        Optional<Userr> optionalUser = userRepository.findByUsername(userr.getUsername());
        if (optionalUser.isPresent()) {
            LOGGER.error("User with username {} already exists in db", optionalUser.get().getUsername());
            throw new DuplicateResourceException(Userr.class.getSimpleName() + " with username: " + optionalUser.get().getUsername());
        }
        else return userRepository.save(userr);
    }

    public Userr insertUser(CaregiverDetailsDTO caregiverDetailsDTO) {
        Userr userr = UserBuilder.toEntity(caregiverDetailsDTO);
        Optional<Userr> optionalUser = userRepository.findByUsername(userr.getUsername());
        if (optionalUser.isPresent()) {
            LOGGER.error("User with username {} already exists in db", optionalUser.get().getUsername());
            throw new DuplicateResourceException(Userr.class.getSimpleName() + " with username: " + optionalUser.get().getUsername());
        }
        else return userRepository.save(userr);
    }

    public Userr updateUser(PatientDetailsDTO patientDetailsDTO) {
        Userr userr = UserBuilder.toEntity(patientDetailsDTO);
        return userRepository.save(userr);
    }
}
